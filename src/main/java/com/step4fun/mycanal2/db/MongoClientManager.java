package com.step4fun.mycanal2.db;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.step4fun.mycanal2.configuration.MongoConfiguration;
import io.dropwizard.lifecycle.Managed;

import java.net.UnknownHostException;

/**
 * Created by stephanekamga on 01/02/15.
 */
public class MongoClientManager implements Managed {

    private String host;
    private DB db;
    private int port;
    private String dbname;
    private MongoClient mongoClient;

    public MongoClientManager(MongoConfiguration configuration) {
        this.host = configuration.getHost();
        this.port = configuration.getPort();
        this.dbname = configuration.getName();
        try {
            mongoClient = new MongoClient( host , port );
            db = mongoClient.getDB(dbname);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void start() throws Exception {

    }

    @Override
    public void stop() throws Exception {
        mongoClient.close();
    }
    public DB getDB()
    {
        return db;
    }
}
