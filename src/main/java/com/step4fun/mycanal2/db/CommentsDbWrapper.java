package com.step4fun.mycanal2.db;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.step4fun.mycanal2.api.model.Comments;
import org.mongojack.DBQuery;
import org.mongojack.JacksonDBCollection;

import java.util.List;

/**
 * Created by stephanekamga on 20/03/15.
 */
public class CommentsDbWrapper {
    private DB db;
    private JacksonDBCollection<Comments, String> coll;
    private static final String COMMENTS_COLLECTION_NAME = "COMMENTS_COLLECTION_NAME";
    public CommentsDbWrapper(DB db)
    {

        this.db = db;
        coll = JacksonDBCollection.wrap(db.getCollection(COMMENTS_COLLECTION_NAME), Comments.class,
                String.class);
    }
    public void createCommentsCollection()
    {
        db.createCollection(COMMENTS_COLLECTION_NAME,new BasicDBObject());
    }
    public void cancelShowsCollection()
    {
        db.getCollection(COMMENTS_COLLECTION_NAME).drop();
    }

    public void insert(List<Comments> modelList)
    {


        coll.insert(modelList);
        System.out.println(modelList.size()+" comments inserted!");
    }

    public void insert(Comments model)
    {
        coll.insert(model);
    }

    public List<Comments> load(int count,long nextId) {

        if(nextId==0)
            return coll.find().sort(new BasicDBObject("$natural", -1)).limit(count).toArray();
        else return coll.find(DBQuery.lessThan("dateTime",nextId)).limit(count).toArray();

    }

//    public List<Comments> loadAll() {
//        JacksonDBCollection<ShowsModel, String> coll = JacksonDBCollection.wrap(db.getCollection(SHOWS_COLLECTION_NAME), ShowsModel.class,
//                String.class);
//        return coll.find().toArray();
//
//    }

//    public boolean update(ProgramsModel model)
//    {
//        JacksonDBCollection<ShowsModel, String> coll = JacksonDBCollection.wrap(db.getCollection(SHOWS_COLLECTION_NAME), ShowsModel.class,
//                String.class);
//        List<ShowsModel> list = coll.find().is("id", model.getId()).toArray();
//        if(list!=null&&!list.isEmpty())
//        {
//            ShowsModel result = list.get(0);
//            result.getModel().setStartTime(model.getStartTime());
//            result.getModel().setEndTime(model.getEndTime());
//            coll.updateById(result.get_id(),result);
//            return true;
//        }
//        return false;
//
//    }

}
