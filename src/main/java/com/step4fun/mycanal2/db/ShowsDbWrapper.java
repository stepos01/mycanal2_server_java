package com.step4fun.mycanal2.db;

import com.google.common.base.Optional;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.step4fun.mycanal2.api.model.ProgramsModel;
import com.step4fun.mycanal2.api.model.ShowsModel;
import org.mongojack.DBCursor;
import org.mongojack.DBQuery;
import org.mongojack.DBUpdate;
import org.mongojack.JacksonDBCollection;

import java.util.List;

/**
 * Created by stephanekamga on 01/02/15.
 */
public class ShowsDbWrapper {


    private DB db;
    private static final String SHOWS_COLLECTION_NAME = "SHOWS_COLLECTION_NAME";
    private JacksonDBCollection<ShowsModel, String> coll;
    public ShowsDbWrapper(DB db)
    {
        this.db = db;
        coll = JacksonDBCollection.wrap(db.getCollection(SHOWS_COLLECTION_NAME), ShowsModel.class,
                String.class);
    }
    public void createShowsCollection()
    {
        db.createCollection(SHOWS_COLLECTION_NAME,new BasicDBObject());
    }
    public void cancelShowsCollection()
    {
        db.getCollection(SHOWS_COLLECTION_NAME).drop();
    }

    public void insert(List<ShowsModel>modelList)
    {
        for(ShowsModel model : modelList)assert (model.getModel().check());

        coll.insert(modelList);
        System.out.println(modelList.size()+" documents inserted!");
    }

    public void insert(ShowsModel model)
    {
        assert (model.getModel().check());

        coll.insert(model);
    }

    public List<ShowsModel> load() {

        return coll.find().is("published", true).toArray();

    }

    public List<ShowsModel> loadAll() {

        return coll.find().toArray();

    }

    public boolean update(ProgramsModel model)
    {

        DBCursor<ShowsModel> id = coll.find().is("model.id", model.getId());
        if(id.hasNext())
        {
            ShowsModel result = id.next();
            result.getModel().setStartTime(model.getStartTime());
            result.getModel().setEndTime(model.getEndTime());
            coll.updateById(result.get_id(), result);
            return true;
        }
        return false;

    }
    public Optional<ProgramsModel> incViewsCount(long id)
    {
        if(coll.find(DBQuery.is("model.id",id)).hasNext())
        {
            coll.update(DBQuery.is("model.id",id), DBUpdate.inc("model.viewsCount"));

            return Optional.of(coll.findOne(DBQuery.is("model.id", id)).getModel());
        }return Optional.absent();
    }

    public Optional<ProgramsModel> modifyLikeCount(long id,int count)
    {
        if(coll.find(DBQuery.is("model.id",id)).hasNext())
        {
            coll.update(DBQuery.is("model.id",id), DBUpdate.inc("model.likesCount",count));

            return Optional.of(coll.findOne(DBQuery.is("model.id", id)).getModel());
        }return Optional.absent();
    }






}
