package com.step4fun.mycanal2.resource;

import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Optional;
import com.mongodb.DB;
import com.step4fun.mycanal2.api.model.ProgramsModel;
import com.step4fun.mycanal2.api.model.ShowsModel;
import com.step4fun.mycanal2.db.ShowsDbWrapper;
import io.dropwizard.jersey.params.IntParam;
import io.dropwizard.jersey.params.LongParam;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephanekamga on 25/01/15.
 */
@Path("/shows")
public class ShowsResource {

    private ShowsDbWrapper wrapper;
    public ShowsResource(DB client)
    {
        wrapper = new ShowsDbWrapper(client);
    }

    @GET
    @Produces("application/json")
    @Timed
    public List<ProgramsModel>getShowsList()
    {
        List<ProgramsModel> result = new ArrayList<ProgramsModel>();
        List<ShowsModel>modelList = wrapper.loadAll();
        for(ShowsModel model : modelList)
        {
             result.add(model.getModel());
        }
        return result;
    }

    @POST
    @Consumes("application/json")
    @Timed
    @Produces("text/plain")
    public String update(@Valid ProgramsModel model)
    {
        wrapper.insert(new ShowsModel().setPublished(false).setModel(model));
        return String.valueOf(true);
    }


    @POST
    @Timed
    @Path("/{show_id}/views")
    @Produces("application/json")
    public ProgramsModel incViews(@PathParam("show_id")LongParam id)
    {
        Optional<ProgramsModel>result = wrapper.incViewsCount(id.get());
        if(result.isPresent())return result.get();
        else throw  new WebApplicationException(Response.Status.NOT_FOUND);
    }

    @POST
    @Timed
    @Path("/{show_id}/like")
    @Produces("application/json")
    public ProgramsModel changeLikes(@PathParam("show_id")LongParam id,@QueryParam("value") Optional<IntParam> count)
    {
        Optional<ProgramsModel>result = wrapper.modifyLikeCount(id.get(), count.or(new IntParam("1")).get());
        if(result.isPresent())return result.get();
        else throw  new WebApplicationException(Response.Status.NOT_FOUND);
    }





}
