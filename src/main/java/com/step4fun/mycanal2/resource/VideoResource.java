package com.step4fun.mycanal2.resource;

import com.codahale.metrics.annotation.Timed;
import com.step4fun.mycanal2.Utils;
import com.step4fun.mycanal2.configuration.Canal2Configuration;
import io.dropwizard.jersey.caching.CacheControl;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by stephanekamga on 23/01/15.
 */
@Path("/stream/")
public class VideoResource implements StreamResource {

    private Client mClient;
    private Canal2Configuration mConfiguration;
    public VideoResource(Client client,Canal2Configuration configuration)
    {
       mClient = client;
        mConfiguration = configuration;
    }




    @GET
    @Timed
    @Produces(value = MediaType.TEXT_PLAIN)
    @CacheControl(maxAge = 10, maxAgeUnit = TimeUnit.MINUTES)
    public String getUrl()
    {
        try {
            return findUri();
        } catch (Exception e)
        {
            throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
        }
    }
    @GET
    @Timed
    @Produces(value = MediaType.TEXT_PLAIN)
    @CacheControl(maxAge = 10, maxAgeUnit = TimeUnit.MINUTES)
    @Path("/{query}")
    public String getUrl2(@QueryParam("query") int query)
    {
        try {
            return findUri();
        } catch (Exception e)
        {
            throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
        }
    }




    @Override
    public String findUri() {

        String result   = null;
        try {
            Document doc = Jsoup.connect("http://www.canal2international.net/live.php").get();
            Elements iframe = doc.getElementsByTag("iframe");
            String vidFrame = null;
            if (iframe.size()!=0)
            {         vidFrame = iframe.get(0).attributes().asList().get(0).getValue(); ;
                Connection connect = Jsoup.connect(vidFrame);
                String html = connect.get().html();
                String[] split = html.split("file: \"");
                String[] split2 = split[1].split("playlist.m3u8");
                result = split2[0]+"playlist.m3u8";
            }
        } catch (IOException e) {
            return "";
        }

        return  Utils.NullOrWhiteSpace(result)?"":result;

//        //ClientConfig config = new ClientConfig();
//
//        WebTarget target = mClient.target(UriBuilder.fromUri(mConfiguration.getMyCanal2PHPWebServer()).build());
//        return target.request().accept(MediaType.TEXT_PLAIN).get(String.class);
////          return mClient.resource(UriBuilder.fromUri(mConfiguration.getMyCanal2PHPWebServer()).build())
////                  .accept(MediaType.TEXT_PLAIN).get(String.class);

    }
}
