package com.step4fun.mycanal2.resource;

import com.codahale.metrics.annotation.Timed;
import com.step4fun.mycanal2.api.controller.MockProvider;
import com.step4fun.mycanal2.api.model.ProgramsModel;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by stephanekamga on 22/01/15.
 */

@Path("/day")
public class DayPlanningResource {





    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Timed
    @Path("/{daynumber}")
    public List<ProgramsModel> getPlanning(@QueryParam("daynumber") String day)
    {

        int dayNumber;
        try {
            dayNumber = Integer.parseInt(day);
        }catch (NumberFormatException e)
        {
            throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
        }
        return MockProvider.loadProgramFor(dayNumber);

    }
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Timed
    public List<ProgramsModel> getAllPlanning()
    {

        return MockProvider.loadAllPrograms();
    }
}
