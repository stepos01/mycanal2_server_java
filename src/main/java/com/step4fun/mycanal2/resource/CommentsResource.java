package com.step4fun.mycanal2.resource;

import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Optional;
import com.mongodb.DB;
import com.step4fun.mycanal2.api.model.Comments;
import com.step4fun.mycanal2.db.CommentsDbWrapper;
import io.dropwizard.jersey.params.IntParam;
import io.dropwizard.jersey.params.LongParam;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by stephanekamga on 20/03/15.
 */

@Path("/comments")
public class CommentsResource {


    private CommentsDbWrapper wrapper;
    public CommentsResource(DB client)
    {
        wrapper = new CommentsDbWrapper(client);
    }


    @POST
    @Timed
    @Consumes("application/json")
    @Produces("application/json")
    public Response newComments(@Valid Comments model)
    {
        wrapper.insert(model);
        return Response.ok().build();
    }
    @GET
    @Timed
    @Produces("application/json")
    public List<Comments> loadComments(@QueryParam("count") Optional<IntParam> count, @QueryParam("next") Optional<LongParam> next)
    {

        return wrapper.load(count.or(new IntParam("20")).get(),next.or(new LongParam("0")).get());

    }

}
