package com.step4fun.mycanal2.resource;

/**
 * Created by stephanekamga on 23/01/15.
 */
public interface StreamResource {

    public String findUri();
}
