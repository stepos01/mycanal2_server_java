package com.step4fun.mycanal2;

import com.step4fun.mycanal2.Command.CancelDBCommand;
import com.step4fun.mycanal2.Command.CreateDBCommand;
import com.step4fun.mycanal2.api.controller.YoutubeClient;
import com.step4fun.mycanal2.configuration.Canal2Configuration;
import com.step4fun.mycanal2.db.MongoClientManager;
import com.step4fun.mycanal2.healthcheck.MongoHealthCheck;
import com.step4fun.mycanal2.healthcheck.ProgramModelHealthCheck;
import com.step4fun.mycanal2.resource.CommentsResource;
import com.step4fun.mycanal2.resource.DayPlanningResource;
import com.step4fun.mycanal2.resource.ShowsResource;
import com.step4fun.mycanal2.resource.VideoResource;
import io.dropwizard.Application;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import javax.ws.rs.client.Client;


/**
 * Created by stephanekamga on 22/01/15.
 */
public class Canal2Application extends Application<Canal2Configuration> {




    public static void main(String[] args) throws Exception {
          new Canal2Application().run(args);
    }
    @Override
    public void initialize(Bootstrap<Canal2Configuration> bootstrap) {
        bootstrap.addCommand(new CancelDBCommand());
        bootstrap.addCommand(new CreateDBCommand());
    }

    @Override
    public void run(Canal2Configuration canal2Configuration, Environment environment) throws Exception {
        final DayPlanningResource dayPlanningResource = new DayPlanningResource();
        final YoutubeClient youtubeClient = new YoutubeClient(canal2Configuration.getYoutubeApiKey());
        final MongoClientManager mongoClientManager = new MongoClientManager(canal2Configuration.getMongoConfiguration());
        final ProgramModelHealthCheck programModelHealthCheck = new ProgramModelHealthCheck(dayPlanningResource.getAllPlanning());
        final MongoHealthCheck mongoHealthCheck = new MongoHealthCheck(mongoClientManager.getDB());
        Client client = new JerseyClientBuilder(environment).using(canal2Configuration.getJerseyClientConfiguration())
                .build(getName());

        environment.lifecycle().manage(youtubeClient);
        environment.lifecycle().manage(mongoClientManager);
         environment.healthChecks().register("modelcheck",programModelHealthCheck);
        environment.healthChecks().register("mongoHealthCheck",mongoHealthCheck);
       // environment.jersey().register(new DayPlanningResource());
        environment.jersey().register(new VideoResource(client,canal2Configuration));
        environment.jersey().register(new ShowsResource(mongoClientManager.getDB()));
        environment.jersey().register(new CommentsResource(mongoClientManager.getDB()));

    }
}
