package com.step4fun.mycanal2.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import com.step4fun.mycanal2.api.model.ProgramsModel;

import java.util.List;

/**
 * Created by stephanekamga on 23/01/15.
 */
public class ProgramModelHealthCheck extends HealthCheck {

    private List<ProgramsModel> mModels;
    public ProgramModelHealthCheck(List<ProgramsModel> models)
    {
       this.mModels = models;
    }

    @Override
    protected Result check() throws Exception {
        for(ProgramsModel model : mModels)
        {
            if(!model.check()){
                return Result.unhealthy("Wrong model show with id:\"+ model.getId()");
            }
            break;
        }

        return Result.healthy();
    }
}
