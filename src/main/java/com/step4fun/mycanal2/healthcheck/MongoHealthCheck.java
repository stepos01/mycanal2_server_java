package com.step4fun.mycanal2.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import com.mongodb.DB;

/**
 * Created by stephanekamga on 01/02/15.
 */
public class MongoHealthCheck extends HealthCheck {

    private DB db;

    public MongoHealthCheck(DB db) {
        this.db = db;
    }

    @Override
    protected Result check() throws Exception {
       db.getCollectionNames();
        return Result.healthy();
    }
}
