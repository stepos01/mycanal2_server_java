package com.step4fun.mycanal2.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by stephanekamga on 22/01/15.
 */
public class Canal2Configuration extends Configuration {

    public Canal2Configuration() {
    }

    public String myCanal2PHPWebServer;
    public String youtubeApiKey;
    public MongoConfiguration mongoConfiguration;

    @JsonProperty("mongo")
    public MongoConfiguration getMongoConfiguration(){return mongoConfiguration;}

    @JsonProperty("myCanal2PHPWebServer")
    public String getMyCanal2PHPWebServer() {
        return myCanal2PHPWebServer;
    }

    @JsonProperty("youtubeApikey")
    public String getYoutubeApiKey() {
        return youtubeApiKey;
    }

    @Valid
    @NotNull
    public JerseyClientConfiguration httpClient;

    @JsonProperty("jerseyClient")
    public void setjerseyClientConfiguration(JerseyClientConfiguration config) {
        httpClient =  config;
    }
    @JsonProperty("jerseyClient")
    public JerseyClientConfiguration getJerseyClientConfiguration() {
        return httpClient;
    }
}
