package com.step4fun.mycanal2.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by stephanekamga on 01/02/15.
 */
public class MongoConfiguration {

    public MongoConfiguration()
    {}
    @NotEmpty
    @JsonProperty("host")
    public String host;

    @NotEmpty
    @JsonProperty("port")
    public int port;

    @NotEmpty
    @JsonProperty("name")
    public String name;

    @JsonProperty("host")
    public String getHost() {
        return host;
    }

    @JsonProperty("port")
    public int getPort() {
        return port;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }
}
