package com.step4fun.mycanal2.Command;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.step4fun.mycanal2.api.controller.MockProvider;
import com.step4fun.mycanal2.api.controller.YoutubeClient;
import com.step4fun.mycanal2.api.model.ProgramsModel;
import com.step4fun.mycanal2.api.model.ShowsModel;
import com.step4fun.mycanal2.configuration.Canal2Configuration;
import com.step4fun.mycanal2.db.ShowsDbWrapper;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.inf.Namespace;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephanekamga on 01/02/15.
 */
public class CreateDBCommand extends ConfiguredCommand<Canal2Configuration> {

    public CreateDBCommand() {
        super("mongocreate", "this command launch the code to create a db mongo with the name specified in the config file");
    }

    @Override
    protected void run(Bootstrap<Canal2Configuration> bootstrap, Namespace namespace, Canal2Configuration configuration) throws Exception {
        MongoClient client = new MongoClient(configuration.getMongoConfiguration().getHost(),configuration.getMongoConfiguration().getPort());
        DB db = client.getDB(configuration.getMongoConfiguration().getName());
        System.out.println("Database "+configuration.getMongoConfiguration().getName()+" created!");
        ShowsDbWrapper showsDbWrapper = new ShowsDbWrapper(db);
        showsDbWrapper.createShowsCollection();
        System.out.println("Collection "+"SHOWS_COLLECTION_NAME"+" created!");


        List<ShowsModel> insert = new ArrayList<ShowsModel>();
        YoutubeClient youtubeClient = new YoutubeClient(configuration.getYoutubeApiKey());
        for(ProgramsModel model : MockProvider.loadShows(youtubeClient))
        {
            insert.add(new ShowsModel().setPublished(true).setModel(model));
        }
        showsDbWrapper.insert(insert);


    }
}
