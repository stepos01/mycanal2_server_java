package com.step4fun.mycanal2.Command;

import com.mongodb.MongoClient;
import com.step4fun.mycanal2.configuration.Canal2Configuration;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.inf.Namespace;

/**
 * Created by stephanekamga on 01/02/15.
 */
public class CancelDBCommand extends ConfiguredCommand<Canal2Configuration> {

    public CancelDBCommand() {
        super("mongodelete", "delete the mongo database");
    }

    @Override
    protected void run(Bootstrap<Canal2Configuration> bootstrap, Namespace namespace, Canal2Configuration configuration) throws Exception {
        MongoClient client = new MongoClient(configuration.getMongoConfiguration().getHost(),configuration.getMongoConfiguration().getPort());
        client.dropDatabase(configuration.getMongoConfiguration().getName());
        System.out.println("Database "+configuration.getMongoConfiguration().getName()+" deleted!");
    }
}
