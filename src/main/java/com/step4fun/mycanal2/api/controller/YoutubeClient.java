package com.step4fun.mycanal2.api.controller;


import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import io.dropwizard.lifecycle.Managed;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephanekamga on 31/01/15.
 */
public class YoutubeClient implements Managed{

    private String apiKey;
    private static  YouTube mYouTube;
    private static final String PART = "snippet";
    public YoutubeClient( String apiKey)
    {
        this.apiKey = apiKey;
        mYouTube = new YouTube.Builder(new ApacheHttpTransport(), new GsonFactory(), new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest httpRequest) throws IOException {

            }
        }).setApplicationName("MyCanal2Android").build();
    }

    public ArrayList<String> getThumbForPlaylist(String playlistId) {
        ArrayList<String> result = new ArrayList<String>();
        try {
            YouTube.PlaylistItems.List request = mYouTube.playlistItems().list("snippet").setKey(apiKey).setPlaylistId(playlistId);
            PlaylistItemListResponse response = request.execute();
            if (response != null&&response.getItems()!=null&&response.getItems().size()>0) {
                List<PlaylistItem> items = response.getItems();
                for(PlaylistItem i : items)
                {
                    if(i.getSnippet().getThumbnails().getMaxres()!=null)
                    {
                        result.add(i.getSnippet().getThumbnails().getMaxres().getUrl());

                    }else if(i.getSnippet().getThumbnails().getHigh()!=null){
                        result.add(i.getSnippet().getThumbnails().getHigh().getUrl());
                    }else if(i.getSnippet().getThumbnails().getDefault()!=null)
                    {
                        result.add(i.getSnippet().getThumbnails().getDefault().getUrl());
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error with playlist id:"+playlistId);
        }
        return result;
    }
    public String getThumbForVideo(String videoId)
    {
        String result = null;


        try {
            YouTube.Videos.List request = mYouTube.videos().list(PART);
            VideoListResponse response = request.setKey(apiKey).setId(videoId).execute();
            if(response!=null&&response.getItems()!=null&&response.getItems().size()>0)
            {
                Video i = response.getItems().get(0);
                if(i.getSnippet().getThumbnails().getMaxres()!=null)
                {
                    result = i.getSnippet().getThumbnails().getMaxres().getUrl();

                }else if(i.getSnippet().getThumbnails().getHigh()!=null){
                    result = i.getSnippet().getThumbnails().getHigh().getUrl();
                }else if(i.getSnippet().getThumbnails().getDefault()!=null)
                {
                    result = i.getSnippet().getThumbnails().getDefault().getUrl();
                }            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error with video id:"+videoId);

        }
        return result;
    }

    @Override
    public void start() throws Exception {

    }

    @Override
    public void stop() throws Exception {
        mYouTube = null;
    }
}
