package com.step4fun.mycanal2.api.controller;

import com.step4fun.mycanal2.api.model.ProgramsModel;
import com.step4fun.mycanal2.api.model.ShowsVideo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

/**
 * Created by stephanekamga on 22/01/15.
 */
public class MockProvider {
    private static final String SECTION_INFORMATION = "Information";
    private static final String SECTION_SPORT = "Sport";
    private static final String SECTION_JEUX = "Jeux";
    private static final String SECTION_DIVERS= "Divertissement";
    private static final String SECTION_MUSIQUE = "Musique";
    private static final String SECTION_SANTE = "Santé";


    public static List<ProgramsModel> loadProgramFor(int day) {
        List<ProgramsModel> models = new ArrayList<ProgramsModel>();
        for(int i = 0;i<day;i++){
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(new Random().nextLong());
            Calendar d = Calendar.getInstance();
            d.setTimeInMillis(new Random().nextLong());
            ProgramsModel model = new ProgramsModel();
            model.setStartTime(c.getTimeInMillis());
            model.setEndTime(d.getTimeInMillis());
            model.setId(i);
            model.setImageUrl("http://placehold.it/350x150");
            models.add(model);
        }

        return models;
    }

    public static List<ProgramsModel> loadShows(YoutubeClient client)
    {
        List<ProgramsModel> models = new ArrayList<ProgramsModel>();

        ProgramsModel model = new ProgramsModel();
        model.setId(1);
        model.setFrequency(7);
        model.setSection(SECTION_INFORMATION);
        model.setSynopsis("\"C’est une émission de débats. Des débats ouverts sur un ring. C’est un combat entre 2 journalistes et un invité. Le but : l’amener à se défendre sur les sujets sensibles autour de sa personne, de ses engagements, de ses positions. Un débat houleux qui permet aux téléspectateurs par ailleurs de bien connaître l’invité et de tirer des leçons de cet entretien que dirige un présentateur.");
        model.setStartTime(7, 20, 30);
        model.setEndTime(7, 22, 0);
        model.setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7cFV0F-FJwXFHFpl0EQMF04").get(0));
        model.setTitle("L'arene");
        model.addShowsVideos(new ShowsVideo().new Builder().IsPlaylist(true).setId("PLe85eUD68s7cFV0F-FJwXFHFpl0EQMF04").Build(client));

        ProgramsModel model2 = new ProgramsModel();
        model2.setId(2);
        model2.setSection(SECTION_INFORMATION);
        model2.setSynopsis("");
        model2.setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7cfSiEkrR2vjLIL6KNxPaPV").get(0));
        model2.setTitle("LA RETRO");
        model2.addShowsVideos(new ShowsVideo().new Builder().IsPlaylist(true).setId("PLe85eUD68s7cfSiEkrR2vjLIL6KNxPaPV").Build(client));



        ProgramsModel model3 = new ProgramsModel();
        model3.setId(3);
        model3.setSection(SECTION_INFORMATION);
        model3.setSynopsis("");
        model3.setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7c0NULgadzLqOFkA4SYcx4A").get(0));
        model3.setTitle("NEWS ROOM");
        model2.addShowsVideos(new ShowsVideo().new Builder().IsPlaylist(true).setId("PLe85eUD68s7c0NULgadzLqOFkA4SYcx4A").Build(client));

        ProgramsModel model4 = new ProgramsModel()
                .setId(4)
                .setSection(SECTION_INFORMATION)
                .setSynopsis("C’est un programme où l’actualité dans  la presse est revisitée par des journalistes. Des personnes ressources sont également associées aux débats. Pour agrémenter cet échange le reportage de la semaine et l’image de la semaine sont aussi objet de débat.")
                .setFrequency(7)
                .setStartTime(7,12,0)
                .setEndTime(7,12,30)
                .setTitle("CANAL PRESSE")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7fvH8CKoWa_T-43z3WrGBIJ").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7fvH8CKoWa_T-43z3WrGBIJ").IsPlaylist(true).Build(client));

        ProgramsModel model5 = new ProgramsModel()
                .setId(5)
                .setSection(SECTION_INFORMATION)
                .setSynopsis("")
                .setTitle("PAROLE D'HOMME")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7exO3klG_Q2lIKNJCBBrGpa").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7exO3klG_Q2lIKNJCBBrGpa").IsPlaylist(true).Build(client));

        ProgramsModel model6 = new ProgramsModel()
                .setId(6)
                .setSection(SECTION_INFORMATION)
                .setSynopsis("")
                .setTitle("TRIBUNE DE L'HISTOIRE")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7em9-K_hadIwKzD5vyHuNCW").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7em9-K_hadIwKzD5vyHuNCW").IsPlaylist(true).Build(client));
        ProgramsModel model7 = new ProgramsModel()
                .setId(7)
                .setSection(SECTION_INFORMATION)
                .setSynopsis("")
                .setTitle("TOUS A L'ANTENNE")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7cX2mE9XbBE-Vl593FWxjyx").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7cX2mE9XbBE-Vl593FWxjyx").IsPlaylist(true).Build(client));
        ProgramsModel model8 = new ProgramsModel()
                .setId(8)
                .setSection(SECTION_INFORMATION)
                .setSynopsis("")
                .setTitle("UN JOUR UN EVENEMENT")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7dMmgKSVmbh7vFS_ecJosJy").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7dMmgKSVmbh7vFS_ecJosJy").IsPlaylist(true).Build(client));


        ProgramsModel model9 = new ProgramsModel()
                .setId(9)
                .setSection(SECTION_MUSIQUE)
                .setSynopsis("")
                .setTitle("MBOA")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7fuWl3z0IZmdcR4Cjuy66iU").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7fuWl3z0IZmdcR4Cjuy66iU").IsPlaylist(true).Build(client));


        ProgramsModel model10 = new ProgramsModel()
                .setId(10)
                .setSection(SECTION_MUSIQUE)
                .setSynopsis("")
                .setTitle("URBAN LIST");

        ProgramsModel model11 = new ProgramsModel()
                .setId(11)
                .setSection(SECTION_SPORT)
                .setSynopsis("Toute l’actualité sur le football. Avec des reportages, les résultats des différents championnats et la chronique de la semaine.")
                .setTitle("GRAND STADE")
                .setImageUrl(client.getThumbForVideo("8dSl9tSwPJo"))
                .addShowsVideos(new ShowsVideo().new Builder().setId("8dSl9tSwPJo").IsPlaylist(false).Build(client))
                .setFrequency(7)
                .setStartTime(1, 22, 0)
                .setEndTime(1, 23, 0);

        ProgramsModel model12 = new ProgramsModel()
                .setId(12)
                .setSection(SECTION_SPORT)
                .setSynopsis("")
                .setTitle("GREENTURF")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7dLBb-CB0aCd13XvDxocE_N").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7dLBb-CB0aCd13XvDxocE_N").IsPlaylist(true).Build(client));



        ProgramsModel model13 = new ProgramsModel()
                .setId(13)
                .setSection(SECTION_SPORT)
                .setSynopsis("")
                .setTitle("SPORT SUR LA 2")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7cL3vxSrbcGSZgV2scCkMrY").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7cL3vxSrbcGSZgV2scCkMrY").IsPlaylist(true).Build(client));


        ProgramsModel model14 = new ProgramsModel()
                .setId(14)
                .setSection(SECTION_SPORT)
                .setSynopsis("")
                .setTitle("AU COEUR DU SPORT")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7cfFqjcFFh_eXnqQ4yNQAip").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7cfFqjcFFh_eXnqQ4yNQAip").IsPlaylist(true).Build(client))
                .setFrequency(14)
                .setStartTime(4, 22, 15)
                .setEndTime(4, 23, 30);


        ProgramsModel model15 = new ProgramsModel()
                .setId(15)
                .setSection(SECTION_SANTE)
                .setSynopsis("C’est un programme d'appel d’aide pour les personnes malades et démunies avec ceci de particulier que le résultat de cette démarche auprès des téléspectateurs est filmée et diffusée ; c'est-à-dire que le processus du traitement de la maladie est présenté aux téléspectateurs.")
                .setTitle("URGENCE")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7fcgF13iac1zFFISDIc2CHc").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7fcgF13iac1zFFISDIc2CHc").IsPlaylist(true).Build(client))
                .setFrequency(7)
                .setStartTime(7, 19, 0)
                .setEndTime(7, 19, 30);

        ProgramsModel model16 = new ProgramsModel()
                .setId(16)
                .setSection(SECTION_DIVERS)
                .setSynopsis("Emission de réveil matinal. Il s’agit d’une émission qui allie détente et informations à travers des reportages basés sur le quotidien des personnes, la revue de la presse,  les informations sportives, l’humour et la musique. Tout ceci dans une ambiance détendue.")
                .setTitle("CANAL MATIN")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7cG1b0-XNU8CcjapZfTOKYr").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7cG1b0-XNU8CcjapZfTOKYr").IsPlaylist(true).Build(client))
                .setFrequency(1)
                .setStartTime(1, 6, 30)
                .setEndTime(1, 8, 30);

        ProgramsModel model17 = new ProgramsModel()
                .setId(17)
                .setSection(SECTION_DIVERS)
                .setSynopsis("C’est l’émission de l’actualité Cinéma dans le monde au Cameroun. Malgré l’absence de salle de Cinéma. Ce programme a le mérite de parler de la production des films camerounais.")
                .setTitle("100% CINE")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7c6k73oxSpmWsANoQ2l7lbc").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7c6k73oxSpmWsANoQ2l7lbc").IsPlaylist(true).Build(client))
                .setFrequency(7)
                .setStartTime(6, 11, 0)
                .setEndTime(6,11, 30);

        ProgramsModel model18 = new ProgramsModel()
                .setId(18)
                .setSection(SECTION_DIVERS)
                .setSynopsis("")
                .setTitle("JAMBO")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7cMHabYREkjBHfdCNziFfJQ").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7cMHabYREkjBHfdCNziFfJQ").IsPlaylist(true).Build(client));

        ProgramsModel model19 = new ProgramsModel()
                .setId(19)
                .setSection(SECTION_DIVERS)
                .setSynopsis("")
                .setTitle("C'COMMENT? On est là!")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7d89y1v0td3clWeYYeYAcXS").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7d89y1v0td3clWeYYeYAcXS").IsPlaylist(true).Build(client));


        ProgramsModel model20 = new ProgramsModel()
                .setId(20)
                .setSection(SECTION_DIVERS)
                .setSynopsis("")
                .setTitle("DANS LE SAHRE")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7d-5W-0cGz2ggJDgqPrHe3-").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7d-5W-0cGz2ggJDgqPrHe3-").IsPlaylist(true).Build(client));



        ProgramsModel model21 = new ProgramsModel()
                .setId(21)
                .setSection(SECTION_DIVERS)
                .setSynopsis("")
                .setTitle("COMPTOIR D'AFRIC")
                .setFrequency(7)
                .setStartTime(6, 11, 30)
                .setEndTime(6,12, 0);

        ProgramsModel model22 = new ProgramsModel()
                .setId(22)
                .setSection(SECTION_DIVERS)
                .setSynopsis("")
                .setTitle("CARAVANE MOBILE")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7cf9feQ56JME0nO97LU6Zkl").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7cf9feQ56JME0nO97LU6Zkl").IsPlaylist(true).Build(client));


        ProgramsModel model23 = new ProgramsModel()
                .setId(23)
                .setSection(SECTION_DIVERS)
                .setSynopsis("")
                .setTitle("SCRATCH YA EYE")
                .setImageUrl(client.getThumbForPlaylist("PLe85eUD68s7egnt1ca84eK5uPVHoW0MC7").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLe85eUD68s7egnt1ca84eK5uPVHoW0MC7").IsPlaylist(true).Build(client));


        ProgramsModel model24 = new ProgramsModel()
                .setId(24)
                .setSection(SECTION_DIVERS)
                .setSynopsis("Découvrez comme vous ne les avez jamais vues, des personnalités du public et du privé de la scène nationale et internationale qui se sont construites grâce à leur persévérance et ardeur au travail. Des reportages inédits, des sujets graves, sur un ton léger, dans une ambiance bon enfant…")
                .setTitle("K-TAPULT")
                .setImageUrl(client.getThumbForPlaylist("PLAYXgQKg0kisyhO6KGAPCtDd1wPe6jiYt").get(0))
                .addShowsVideos(new ShowsVideo().new Builder().setId("PLAYXgQKg0kisyhO6KGAPCtDd1wPe6jiYt").IsPlaylist(true).Build(client));


        models.add(model);
        models.add(model2);
        models.add(model3);
        models.add(model4);
        models.add(model5);
        models.add(model6);
        models.add(model7);
        models.add(model8);
        models.add(model9);
        models.add(model10);
        models.add(model11);
        models.add(model12);
        models.add(model13);
        models.add(model14);
        models.add(model15);
        models.add(model16);
        models.add(model17);
        models.add(model18);
        models.add(model19);
        models.add(model20);
        models.add(model21);
        models.add(model22);
        models.add(model23);
        models.add(model24);
        return models;
    }

    public static List<ProgramsModel> loadAllPrograms() {
        return loadProgramFor(7);
    }
}
