package com.step4fun.mycanal2.api.model;


import org.hibernate.validator.constraints.NotEmpty;

public class Comments {
    @NotEmpty
    private String username;
    @NotEmpty
    private String comment;

    private String thumbUrl;

    private long dateTime;

    public Comments() {
    }

    public String _id;

    public Comments(String username, String comment, String thumbUrl, long dateTime) {
        this.username = username;
        this.comment = comment;
        this.thumbUrl = thumbUrl;
        this.dateTime = dateTime;
    }






    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }
}
