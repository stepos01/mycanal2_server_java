package com.step4fun.mycanal2.api.model;

import com.step4fun.mycanal2.api.controller.YoutubeClient;

import java.util.List;
import java.util.Random;

/**
 * Created by stephanekamga on 02/02/15.
 */
public class ShowsVideo {

    public String videoId;

    public boolean isPlaylist;

    public String imageUrl;

    public boolean isPlaylist() {
        return isPlaylist;
    }

    public void setPlaylist(boolean isPlaylist) {
        this.isPlaylist = isPlaylist;
    }

    public String getVideoId() {
        return videoId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public ShowsVideo() {
    }

    public class Builder{

        public Builder setId(String id)
        {
             setVideoId(id);return this;
        }
        public Builder IsPlaylist(boolean pIsPlaylist)
        {
            setPlaylist(pIsPlaylist);return this;
        }
        public  ShowsVideo Build(YoutubeClient client)
        {
           if(!isPlaylist)setImageUrl(client.getThumbForVideo(videoId));
            else {
               List<String> thumbs = client.getThumbForPlaylist(videoId);
               Random r = new Random();
               setImageUrl(thumbs.get(r.nextInt(thumbs.size())));
           }
            return ShowsVideo.this;

        }
    }
}
