package com.step4fun.mycanal2.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.step4fun.mycanal2.Utils;
import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by stephanekamga on 17/01/15.
 */
public class ProgramsModel {
    private String synopsis;
    private long startTime;
    private long endTime;
    private String title;
    private long id;
    private String imageUrl;
    private long reminderId;
    private boolean isReminderSet;
    private int type;
    private String section;
    private int viewsCount;
    private int likesCount;
    private ArrayList<ShowsVideo> showsVideos;
    /**
     * frequency of the event in days
     */
    private int frequency;

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getFrequency() {
        return frequency;
    }

    public ProgramsModel setFrequency(int frequency) {
        this.frequency = frequency;
        return this;
    }

    public void setShowsVideos(ArrayList<ShowsVideo> showsVideos) {
        this.showsVideos = showsVideos;
    }

    @JsonIgnore
    public ProgramsModel addShowsVideos(ShowsVideo pShowsVideo)
    {
        if(showsVideos==null)showsVideos = new ArrayList<ShowsVideo>();
        showsVideos.add(pShowsVideo);
        return this;
    }

    public long getId() {
        return id;
    }

    public ProgramsModel setId(long id) {
        this.id = id;return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public ProgramsModel setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;return this;
    }




    public String getSection() {
        return section;
    }

    public ProgramsModel setSection(String section) {
        this.section = section;
        return this;
    }

    public boolean check()
    {
       if(Utils.NullOrWhiteSpace(String.valueOf(id)))return false;
        if(Utils.NullOrWhiteSpace(String.valueOf(title)))return false;
        if(Utils.NullOrWhiteSpace(String.valueOf(section)))return false;
        return true;


    }



    public ArrayList<ShowsVideo> getShowsVideos() {
        return showsVideos;
    }

    public long getStartTime() {
        return startTime;
    }

    public ProgramsModel setStartTime(long startTime) {
        this.startTime = startTime;return this;
    }
    public ProgramsModel setStartTime(int day,int hour,int minute)
    {
        this.startTime = getTime(day,hour,minute);return this;
    }
    public ProgramsModel setEndTime(int day,int hour,int minute)
    {
        this.endTime = getTime(day,hour,minute);return this;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public ProgramsModel setSynopsis(String synopsis) {
        this.synopsis = synopsis;return this;
    }

    public String getTitle() {
        return title;
    }

    public ProgramsModel setTitle(String title) {
        this.title = title;return this;
    }

    public int getType() {
        return type;
    }

    public ProgramsModel setType(int type) {
        this.type = type;return this;
    }

    public ProgramsModel() {
    }
    public ProgramsModel(String section, int type)
    {
        this.section = section;
        this.type = type;
    }
    public boolean isReminderSet() {
        return isReminderSet;
    }

    public ProgramsModel setReminderSet(boolean isReminderSet) {
        this.isReminderSet = isReminderSet;return this;
    }

    public long getReminderId() {
        return reminderId;
    }

    public ProgramsModel setReminderId(long reminderId) {
        this.reminderId = reminderId;return this;
    }
    public static long getTime(int day,int hour,int min)
    {
        DateTime dateTime = new DateTime();
        return dateTime.withDayOfWeek(day).withHourOfDay(hour).withMinuteOfHour(min).toDate().getTime();
    }

    public int getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }
}
