package com.step4fun.mycanal2.api.model;

import java.util.ArrayList;

/**
 * Created by stephanekamga on 28/01/15.
 */
public class ShowsDetailsModel{
    private ArrayList<String> mImageUrlList;
    private String mVideoId;
    private String mTitle;
    private String mDescription;
    private String mTime;
    private boolean isPlayList;



//    /*
//                    Android specific
//             */
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeStringList(mImageUrlList);
//        dest.writeString(mVideoId);
//        dest.writeString(mTitle);
//        dest.writeString(mDescription);
//        dest.writeString(mTime);
//        dest.writeInt(isPlayList ? 1 : -1);
//
//
//    }
//    public static final Creator<ShowsDetailsModel> CREATOR
//            = new Creator<ShowsDetailsModel>() {
//        public ShowsDetailsModel createFromParcel(Parcel in) {
//            return new ShowsDetailsModel(in);
//        }
//
//        public ShowsDetailsModel[] newArray(int size) {
//            return new ShowsDetailsModel[size];
//        }
//    };
//
//    private ShowsDetailsModel(Parcel in) {
//        in.readStringList(mImageUrlList);
//        mVideoId = in.readString();
//        mTitle = in.readString();
//        mDescription = in.readString();
//        mTime = in.readString();
//        isPlayList = in.readInt()==1;
//    }
//
//
//    /*
//            end android shit
//     */

    public Boolean getIsPlayList() {
        return isPlayList;
    }

    public void setIsPlayList(Boolean isPlayList) {
        this.isPlayList = isPlayList;
    }

    public ArrayList<String> getImageUrl() {
        return mImageUrlList;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String mTime) {
        this.mTime = mTime;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public void setImageUrl(ArrayList<String> mImageUrk) {
        this.mImageUrlList = mImageUrk;
    }

    public String getVideoId() {
        return mVideoId;
    }

    public void setVideoId(String videoId) {
        this.mVideoId = videoId;
    }

    public ShowsDetailsModel() {
    }

    public ShowsDetailsModel(String videoid, ArrayList<String> mImageUrlList) {
        this.mVideoId = videoid;
        this.mImageUrlList = mImageUrlList;
    }

    public ShowsDetailsModel(ArrayList<String> mImageUrlList) {
        this.mImageUrlList = mImageUrlList;
    }


    public ShowsDetailsModel(String mDescription, ArrayList<String> mImageUrlList, String mTime, String mTitle, String videoId) {
        this.mDescription = mDescription;
        this.mImageUrlList = mImageUrlList;
        this.mTime = mTime;
        this.mTitle = mTitle;
        this.mVideoId = videoId;
    }

    public ShowsDetailsModel(String mTitle, String mDescription, ArrayList<String> mImageUrlList, String mTime) {
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mImageUrlList = mImageUrlList;
        this.mTime = mTime;
    }


}
