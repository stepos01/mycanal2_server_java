package com.step4fun.mycanal2.api.model;

/**
 * Created by stephanekamga on 01/02/15.
 */
public class ShowsModel {

    private String _id;
    private boolean published;
    private ProgramsModel model;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public ProgramsModel getModel() {
        return model;
    }

    public ShowsModel setModel(ProgramsModel model) {
        this.model = model;return this;
    }

    public boolean isPublished() {
        return published;
    }

    public ShowsModel setPublished(boolean published) {
        this.published = published;
        return this;
    }



}
